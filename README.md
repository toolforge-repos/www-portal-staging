# Portal Staging Build

This script creates a production build of the [www.wikipedia.org](www.wikipedia.org) page and other project portals every 15 minutes. 

Since Node 6 is required to produce the build, the script is run inside the Kubernetes container. To avoid the complexities of setting up cron with Kubernetes, a basic javascript `setInterval` is used to run the build steps every 15 minutes. 

Since the page is static HTML/CSS/JS, it's served through the tools-static.wmflabs.org domain. 

[http://tools-static.wmflabs.org/www-portal-staging/wikipedia.org](http://tools-static.wmflabs.org/www-portal-staging/wikipedia.org)