const exec = require('child_process').execSync;
const fs = require('fs');
const log = fs.createWriteStream('portal.log');

const runBuild = function(){

    if ( !fs.existsSync('portals') ) {  
        exec('git clone https://gerrit.wikimedia.org/r/wikimedia/portals portals');
    }
    
    log.write( exec('cd portals && git reset --hard HEAD', {stdio: 'pipe'}).toString() );
    log.write( exec('cd portals && git pull origin master', {stdio: 'pipe'}).toString() );  
    log.write( exec('cd portals && npm install npm --save-dev', {stdio: 'pipe'}).toString() );  
    log.write( exec('cd portals && ./node_modules/.bin/npm install', {stdio: 'pipe'}).toString() );  
    log.write( exec('cd portals && ./node_modules/.bin/npm run build-all-portals', {stdio: 'pipe'}).toString() );  
    log.write( exec('rsync -av --delete portals/prod/ ../static/', {stdio: 'pipe'}).toString() );
    log.end();
}

runBuild(); 

setTimeout(runBuild, 900000);